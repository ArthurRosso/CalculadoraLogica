package robolab.calculadoralogica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	public Button btlim, btap, btenv, btp, btq, btr, bte, btou, btxor, btimp, btnor, btnand, btequi, btpar1, btpar2, btnao;

	public TextView display;

	public CheckBox cbp, cbq, cbr;

	public boolean p = false, q = false, r = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	private void botaoLim(View v) {
		display.setText("");
	}

	private void botaoAp(View v) {
		String text = display.getText().toString();
		text = text.substring(0, text.length() - 1);

		display.setText(text);
	}

	public void setP(View v) {
		p = cbp.isChecked();
	}

	public void setQ(View v) {
		q = cbq.isChecked();
	}

	public void setR(View v) {
		r = cbr.isChecked();
	}

	private void botaoP(View v) {
		display.setText(display.getText().toString() + btp.getText());
	}

	private void botaoQ(View v) {
		display.setText(display.getText().toString() + btq.getText());
	}

	private void botaoR(View v) {
		display.setText(display.getText().toString() + btr.getText());
	}

	private void botaoE(View v) {
		display.setText(display.getText().toString() + bte.getText());
	}

	private void botaoOu(View v) {
		display.setText(display.getText().toString() + btou.getText());
	}

	private void botaoXor(View v) {
		display.setText(display.getText().toString() + btxor.getText());
	}

	private void botaoImp(View v) {
		display.setText(display.getText().toString() + btimp.getText());
	}

	private void botaoNor(View v) {
		display.setText(display.getText().toString() + btnor.getText());
	}

	private void botaoNand(View v) {
		display.setText(display.getText().toString() + btnand.getText());
	}

	private void botaoEqui(View v) {
		display.setText(display.getText().toString() + btequi.getText());
	}

	private void botaoPar1(View v) {
		display.setText(display.getText().toString() + btpar1.getText());
	}

	private void botaoPar2(View v) {
		display.setText(display.getText().toString() + btpar2.getText());
	}

	private void botaoNao(View v) {
		display.setText(display.getText().toString() + btnao.getText());
	}

	private void botaoEnv(View v) {
		// ArrayList <String> text = new ArrayList<> ();
		// ArrayList <String> resp = new ArrayList<> ();

		char[] text = display.getText().toString().toCharArray();
		char[] resp;
		int cont = 0;

		for (int i = 0; i < text.length; i++) {
			if ((text[i] == '(') || (text[i] == ')')) {
				cont++;
			}
		}

		if ((cont % 2 != 0) || (cont == 0)) {
			display.setText("Expressão Inválida!");
			return;
		}

		for (int i = 0; i < text.length; i++) {
			if (text[i] == 'P') {
				if (p) {
					text[i] = 'V';
				} else {
					text[i] = 'F';
				}
			}
			if (text[i] == 'Q') {
				if (q) {
					text[i] = 'V';
				} else {
					text[i] = 'F';
				}
			}
			if (text[i] == 'R') {
				if (r) {
					text[i] = 'V';
				} else {
					text[i] = 'F';
				}
			}
		}

		resp = resolver(text);

		display.setText(resp.toString());
	}

	public static char[] resolver(char[] text) {

		char[] text2 = new char[text.length];

		char result = ' ';
		int par1 = 0, par2 = 0;
		boolean terminou = false;
		char[] sb = new char[text.length];

		while (!terminou) {
			for (int y = 0; y < text.length; y++) {
				if ('(' == text[y]) {
					par1 = y;
				}
				if (')' == text[y]) {
					par2 = y;
				}
			}
			for (int i = 0; i < par2 - par1; i++) {
				if ((text[par1 + i] != '(') && (text[par1 + i] != ')')) {
					sb[i] = text[par1 + i];
				}
			}

			result = operar(sb);

			if (!terminou) {
				for (int j = 0; j < par1; j++) {
					text2[j] = text[j];
				}

				text2[par1] = result;

				for (int z = 1; z < text.length - par2; z++) {
					text2[par1 + z] = text[par2 + z];
				}

				if (!terminou) {
					terminou = true;
				}
			}

			if (!terminou) {
				terminou = true;
			}

			text = text2;
		}
		return text;
	}

	public static char operar(char[] sb) {
		char[] text3 = sb;
		char retorno = ' ';

		for (int i = 0; i < text3.length; i++) {
			if ((text3[i] != 'V') && (text3[i] != 'F')) {
				switch (text3[i]) {

					case '∧':
						if ((text3[1] == 'V') && (text3[3] == 'V')) {
							retorno = 'V';
						} else {
							retorno = 'F';
						}
						break;

					case '∨':
						if ((text3[1] == 'F') && (text3[3] == 'F')) {
							retorno = 'F';
						} else {
							retorno = 'V';
						}
						break;

					case '⊻':
						if ((text3[1] != text3[3])) {
							retorno = 'V';
						} else {
							retorno = 'F';
						}
						break;

					case '→':
						if ((text3[1] == 'V') && (text3[3] == 'F')) {
							retorno = 'F';
						} else {
							retorno = 'V';
						}
						break;

					case '↓':
						if ((text3[1] == 'F') && (text3[3] == 'F')) {
							retorno = 'V';
						} else {
							retorno = 'F';
						}
						break;

					case '↑':
						if ((text3[1] == 'V') && (text3[3] == 'F')) {
							retorno = 'F';
						} else {
							retorno = 'V';
						}
						break;

					case '↔':
						if ((text3[1] == text3[3])) {
							retorno = 'V';
						} else {
							retorno = 'F';
						}
						break;

					case '~':
						if (text3[2] == 'V') {
							retorno = 'F';
						} else {
							retorno = 'V';
						}
						break;
				}
			}
		}
		return retorno;
	}
}
